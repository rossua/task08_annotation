package com.pikulyk;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

public class ClassInfo {

  public void printAllClassInfo(Object object) {
    Class<?> clazz = getClassInfo(object);
    printClassInfo(clazz);
    printClassConstructors(getClassConstructors(clazz));
    printClassFields(getClassFields(clazz));
    printClassMethods(getClassMethods(clazz));
  }

  private static Class<?> getClassInfo(Object obj) {
    return obj.getClass();
  }

  public void printClassInfo(Class<?> clazz) {
    System.out.println("Class info: "
        + "\nClass simple name: " + clazz.getSimpleName()
        + "\nClass name: " + clazz.getName()
        + "\nParent class: " + clazz.getSuperclass().getName());
  }

  private Constructor[] getClassConstructors(Class<?> clazz) {
    return clazz.getDeclaredConstructors();
  }

  public void printClassConstructors(Constructor[] constructors) {
    System.out.println("Constructors: ");
    for (Constructor constructor : constructors) {
      Class[] paramTypes = constructor.getParameterTypes();
      System.out.println("Constructor name: " + constructor.getName()
          + ", parameter types: " + Arrays.toString(paramTypes));
    }
  }

  private Field[] getClassFields(Class<?> clazz) {
    return clazz.getDeclaredFields();
  }

  public void printClassFields(Field[] fields) {
    System.out.println("Fields: ");
    for (Field field : fields) {
      System.out.println("Name: " + field.getName()
          + ", type: " + field.getType().getSimpleName());
    }
  }

  private Method[] getClassMethods(Class<?> clazz) {
    return clazz.getDeclaredMethods();
  }

  public void printClassMethods(Method[] methods) {
    System.out.println("Methods: ");
    for (Method method : methods) {
      Class[] paramTypes = method.getParameterTypes();
      System.out.println("Name: " + method.getName()
          + ", return type: " + method.getReturnType().getSimpleName()
          + ", parameter types: " + Arrays.toString(paramTypes));
    }
  }
}
