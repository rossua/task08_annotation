package com.pikulyk;

import com.pikulyk.annotation.MyAnnotation;
import com.pikulyk.annotation.MyClass;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {

  private static void printAnnotateFields(Object o) {
    Class<?> clazz = o.getClass();
    System.out.println("Annotated fields: ");
    for (Field field : clazz.getDeclaredFields()) {
      if (field.isAnnotationPresent(MyAnnotation.class)) {
        System.out.println("Name: " + field.getName()
            + ", type: " + field.getType().getSimpleName());
      }
    }
  }

  private static void printAnnotationValue(Object o) {
    Class<?> clazz = o.getClass();
    for (Field field : clazz.getDeclaredFields()) {
      if (field.isAnnotationPresent(MyAnnotation.class)) {
        System.out.println(
            "Annotation value: " + field.getAnnotation(MyAnnotation.class).someDescription());
      }
    }
  }

  private static void invokeMyMethods(MyClass myClass) {
    Class clazz = myClass.getClass();
    try {
      Method method = clazz.getDeclaredMethod("myMethod", String[].class);
      method.invoke(myClass, (Object) new String[]{"Some", "String"});
      method = clazz.getDeclaredMethod("myMethod", String.class, int[].class);
      method.invoke(myClass, "hello", new int[]{9, 3, 5});
    } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
      e.printStackTrace();
    }
  }

  public static void main(String[] args) {
    MyClass myClass = new MyClass();
    Main.printAnnotateFields(myClass);
    Main.invokeMyMethods(myClass);
    Main.printAnnotationValue(myClass);
    new ClassInfo().printAllClassInfo(myClass);
  }
}
