package com.pikulyk.annotation;

import java.lang.reflect.Array;
import java.util.Arrays;

public class MyClass {

  @MyAnnotation(someDescription = "name", counter = 0)
  String name;

  @MyAnnotation(counter = 3)
  int counter;

  private int n;

  public MyClass() {
  }

  public MyClass(String name, int counter) {
    this.name = name;
    this.counter = counter;
  }

  public String getName() {
    return name;
  }

  public int getCounter() {
    return counter;
  }

  public void myMethod(String a, int... args) {
    System.out.print("Simple method that prints parameters: " + a + ", ");
    System.out.println(Arrays.toString(args));
  }

  public void myMethod(String... args) {
    System.out.print("Simple method that prints parameters: ");
    System.out.println(Arrays.toString(args));
  }

  @Override
  public String toString() {
    return "Name: " + name
        + "\nCounter" + counter;
  }
}
